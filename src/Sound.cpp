#include "Sound.h"
#include <iostream>

Sound::Sound() {

	// basic constructor, initialize variables

    // needed to initialize the mixer
	m_iFrequency = 44100;
	m_iFormat = AUDIO_S16SYS;
	m_iChannels = 2;
	m_iChunkSize = 1024;

	// whether or not the music is playing
	m_bMusicPlaying = false;

    // if the mixer is open and available
	m_bMixerOpen = false;

    // number of channels available for mixing sound effects
	m_iSoundChannels = 8;

}

Sound::~Sound() {
	// destructor, delete dynamically allocated memory
}

bool Sound::Init(int frequency, Uint16 format, int channels, int chunksize) {

	// copy the passed-in values to member variables
	m_iFrequency = frequency;
	m_iFormat = format;
	m_iChannels = channels;
	m_iChunkSize = chunksize;
	m_iChannel = 0;

	// initialize SDL mixer, return false if it failed
	if (Mix_OpenAudio(m_iFrequency,m_iFormat,m_iChannels,m_iChunkSize) != 0) {
		return false;
	}

//	// load up test music
//	mMusics.push_back(Mix_LoadMUS(TestSong));
//	mMusics.push_back(Mix_LoadMUS(TestSong2));
//
//	mSounds.push_back(Mix_LoadWAV(ShootSound));
//	mSounds.push_back(Mix_LoadWAV(BoomSound));

	// success, return true
	m_bMixerOpen = true;
	return true;

}

bool Sound::IsPlaying() {

	return m_bMusicPlaying;

}

bool Sound::PlayMusic(MusicType music) {

    // stop current music if it is playing (not needed?)
    StopMusic();

    // try to play the music
    if( Mix_PlayMusic(mMusics[music],-1) == -1 ) {

        // if it failed, music isn't playing
        m_bMusicPlaying = false;

        // report failure
        return false;
    }

    // succeeded, music is playing
    m_bMusicPlaying = true;

    // report success
    return true;
}

bool Sound::PlayMusic(std::string filename) {

    // stop current music if it is playing (not needed?)
    StopMusic();

    // try to play the music
    if( Mix_PlayMusic(Mix_LoadMUS(filename.c_str()),-1) == -1 ) {

        // if it failed, music isn't playing
        m_bMusicPlaying = false;

        // report failure
        return false;
    }

    // succeeded, music is playing
    m_bMusicPlaying = true;

    // report success
    return true;
}

bool Sound::PlaySound(SoundType sound) {

    // play the selected sound on the next available sound channel
    Mix_PlayChannel(m_iChannel,mSounds[sound],0);

    // go to the next channel
    m_iChannel++;

    // if we went past the number of channels, reset to 0
    if (m_iChannel > m_iChannels) { m_iChannel=0; }

    // report success
    return true;
}

void Sound::StopMusic() {

	// stop the music
	if (m_bMixerOpen) {
	    if (Mix_PlayingMusic()) Mix_HaltMusic();
	}

	// set the music playing boolean to false
	m_bMusicPlaying = false;

}

void Sound::StopSound() {

	// stop the sound
	if (m_bMixerOpen ) {
	    for (int i = 0; i < m_iChannels; i++) {
            Mix_HaltChannel(i);
	    }
	}
}

void Sound::Quit() {

	StopMusic();
	StopSound();

    // do we need to free the music and sound clips?

	//Mix_CloseAudio();
}
