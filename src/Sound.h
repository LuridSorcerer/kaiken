/* Sound
 *
 * Handles the music and sound effects.
 *
 * Unfortunately, only one music track and one sound effect can be
 * loaded and played at a time. This makes it easy for me to code it
 * without worrying about pesky memory leaks.
 *
 * TODO:
 * 	- find a non-leaky way to have multiple music tracks and sound effects.
 * 	- When the sound system fails to init, there's a segfault caused here on exit
 */

#ifndef SOUND_H
#define SOUND_H

#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>
#include <string>
#include <vector>

/* TODO: Currently the engine is limited to one music file and one sound
 * effect file at a time. I hope to find some way to remove this
 * limitation in the future, but I'd rather put my effort someplace else
 * for the time being, because dispite its limits, this class does work
 * and no longer leaks memory like crazy on every file play.
 */

#include "Resources.h"
/* KLUDGE: Why in the hell does this class need to load up the test music
 * in its Init function? This should be handled by the class calling it,
 * not this class itself. The game will define what music the game will
 * use, not the sound engine, which should be as generic as possible.
 */

enum MusicType {
    MUSIC_TEST1,
    MUSIC_TEST2
};

enum SoundType {
    SOUND_SHOOT,
    SOUND_BOOM,
    SOUND_JUMP
};

class Sound {

	private:

		int m_iFrequency;
		Uint16 m_iFormat;
		int m_iChannels;
		int m_iChunkSize;

		bool m_bMusicPlaying;
		//bool m_bSoundPlaying;

		bool m_bMixerOpen;

		//Mix_Music* mMusic;
		//Mix_Chunk* mSound;

        std::vector<Mix_Music*> mMusics;
        std::vector<Mix_Chunk*> mSounds;

        int m_iSoundChannels; // number of available mixing channels
        int m_iChannel; // next available sound channel

	public:
		Sound();
		~Sound();

		bool Init(int frequency, Uint16 format, int channels, int chunksize);
		bool IsPlaying();

		bool PlayMusic(std::string filename);
		//bool PlaySound(std::string filename, bool loop);

		bool PlayMusic(MusicType);
        bool PlaySound(SoundType);

		void StopMusic();
		void StopSound();

		void Quit();

};

#endif
