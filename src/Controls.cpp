/*  Controls: a structure for holding which controls are being employed.
    Typically, this would refer to buttons. The data in this class,
    however, is decoupled from the actual button-reading mechanisms.
*/

#include "Controls.h"

Controls::Controls() {

    // initialize buttons
    for (int i = 0; i < BUTTON_COUNT; i++){
        buttons[i] = false;
    }

    // initialize mouse position
    mouseX = 0;
    mouseY = 0;

}

void Controls::setButton(Button btn, bool pressed) { buttons[btn] = pressed; }

bool Controls::getButton(Button btn) { return buttons[btn]; }

void Controls::setMousePosition(int x, int y) { mouseX = x; mouseY = y; }

int Controls::getMouseX() { return mouseX; }

int Controls::getMouseY() { return mouseY; }

Point3D Controls::getMousePosition() {
    Point3D pos;
    pos.x = float(mouseX);
    pos.y = float(mouseY);
    return pos;
}
