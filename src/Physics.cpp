/* Physics
 * A class, chiefly of static functions, that performs various operations
 * related to physics.
 */

#include "Physics.h"
//#include <iostream>

CollisionType Physics::CheckBoxCollision(GameObject* mover, GameObject* target) {

    // Get the bounds of the mover and target
    SDL_Rect m_rect = mover->getBounds();
    SDL_Rect t_rect = target->getBounds();

    // get bounds of current moving object
    float m_top = m_rect.y;
    float m_bottom = m_rect.y+m_rect.h;
    float m_left = m_rect.x;
    float m_right = m_rect.x+m_rect.w;

    // get previous bounds of current moving object
    float m_prev_top = mover->getPrevLocation().y;
    float m_prev_bottom = mover->getPrevLocation().y + m_rect.h;
    float m_prev_left = mover->getPrevLocation().x;
    float m_prev_right = mover->getPrevLocation().x + m_rect.w;

    // get bounds of current collison target
    float t_top = t_rect.y;
    float t_bottom = t_rect.y + t_rect.h;
    float t_left = t_rect.x;
    float t_right = t_rect.x + t_rect.w;

    // check if a collison exists
    // if the edges don't overlap, there's no collision
    if ( m_top > t_bottom ||
         m_bottom < t_top ||
         m_left > t_right ||
         m_right < t_left) {

        return COLLISION_NONE;

    }

    // there is a collision, we'll determine from whence it came.

    // if previously the object was above, it landed on top
    if (m_prev_bottom <= t_top) {
        return COLLISION_TOP;
    }

    // if previously the object was below, it hit the bottom
    if (m_prev_top >= t_bottom) {
        return COLLISION_BOTTOM;
    }

    // if previously the object was to the left, it hit the left
    if (m_prev_right <= t_left) {
        return COLLISION_LEFT;
    }

    // if previously the object was to the right, it hit the right
    /// TODO: Why can I never get this quite right?
    if (m_prev_left < t_right) {
        return COLLISION_RIGHT;
    }

    // this line is impossible to reach... in theory, anyway
    return COLLISION_NONE;

}

/// TODO: This function is kludged to hell and back.
/// Please cleanly rewrite it. Please.
CollisionType Physics::CheckCircleCollision(GameObject* mover, GameObject* target) {

    // get difference between x positions
    float distx = mover->getLocation().x-(mover->getBounds().w/2) - target->getLocation().x-(target->getBounds().w/2);

    // get difference between y locations
    float disty = mover->getLocation().y-(mover->getBounds().h/2) - target->getLocation().y-(target->getBounds().h/2);

    // get combined radii
    float radii = mover->getBounds().w/2 + target->getBounds().w/2;

    // if (squared distances combined) is less than (combined radii squared)
    if ( (distx*distx) + (disty*disty) < (radii*radii) ) {

        // collision has occured
        // we'll worry about which type of collision later
        return COLLISION_TOP;

    } else {

        // no collision detected
        return COLLISION_NONE;
    }

}

void Physics::CheckBlockCollisions(Actor* mover, std::vector<Block> blocks) {

    // Do collision checks for the actor against each block
    // WARNING: May contain hard-coded kludges
    for (uint32_t i = 0; i < blocks.size(); i++) {

        // check for which direction the collision occurred in
        CollisionType col = Physics::CheckBoxCollision(mover,&blocks[i]);

        // If there's a collision, handle the collision according to
        // the direction in which it happened.
        if (col != COLLISION_NONE) {
            switch (col) {

                // landed on top.
                case COLLISION_TOP:
                    // put the actor on top of the block, tell them they are on the ground
                    mover->setLocation(Point3D(mover->getLocation().x,blocks[i].getLocation().y-mover->getBounds().h,mover->getLocation().z));
                    mover->setMomentum(Point3D(mover->getMomentum().x,0,mover->getMomentum().z));
                    mover->setOnGround(true);
                    break;

                // hit head against the bottom
                case COLLISION_BOTTOM:
                    // don't let actor go up through the block: put them at the edge and stop their upward momentum
                    mover->setLocation(Point3D(mover->getLocation().x,blocks[i].getLocation().y+blocks[i].getBounds().h+1,mover->getLocation().z));
                    mover->setMomentum(Point3D(mover->getMomentum().x,0,mover->getMomentum().z));
                    break;

                // crashed into the left side of the block
                case COLLISION_LEFT:
                    // put them at the left edge and stop their lateral momentum
                    mover->setLocation(Point3D(blocks[i].getBounds().x-mover->getBounds().w,mover->getLocation().y,mover->getLocation().z));
                    mover->setMomentum(Point3D(0,mover->getMomentum().y,mover->getMomentum().z));
                    break;

                // smacked into the right edge of the block
                case COLLISION_RIGHT:
                    // stop them at the right edge and stop their lateral momentum
                    mover->setLocation(Point3D(blocks[i].getBounds().x+blocks[i].getBounds().w,mover->getLocation().y,mover->getLocation().z));
                    /// KLUDGE: Only reset momentum if it is moving toward the block.
                    if (mover->getMomentum().x < 0 )
                        mover->setMomentum(Point3D(0,mover->getMomentum().y,mover->getMomentum().z));
                    break;

                // No collision? Don't do anything.
                default:
                    break;
            } // switch (col)

        } // if(collision occurred)

    } // for (each block)

}

void Physics::CheckBlockCollisions(Actor* mover, std::list<Block*> blocks) {

    // Do collision checks for the actor against each block
    // WARNING: May contain hard-coded kludges
    //for (uint32_t i = 0; i < blocks.size(); i++) {
    std::list<Block*>::iterator blocksi;
    for (blocksi=blocks.begin();blocksi!=blocks.end();blocksi++) {

        // check for which direction the collision occurred in
        CollisionType col = Physics::CheckBoxCollision(mover,(*blocksi));

        // If there's a collision, handle the collision according to
        // the direction in which it happened.
        if (col != COLLISION_NONE) {
            switch (col) {

                // landed on top.
                case COLLISION_TOP:
                    // put the actor on top of the block, tell them they are on the ground
                    mover->setLocation(Point3D(mover->getLocation().x,(*blocksi)->getLocation().y-mover->getBounds().h,mover->getLocation().z));
                    mover->setMomentum(Point3D(mover->getMomentum().x,0,mover->getMomentum().z));
                    mover->setOnGround(true);
                    break;

                // hit head against the bottom
                case COLLISION_BOTTOM:
                    // don't let actor go up through the block: put them at the edge and stop their upward momentum
                    mover->setLocation(Point3D(mover->getLocation().x,(*blocksi)->getLocation().y+(*blocksi)->getBounds().h+1,mover->getLocation().z));
                    mover->setMomentum(Point3D(mover->getMomentum().x,0,mover->getMomentum().z));
                    break;

                // crashed into the left side of the block
                case COLLISION_LEFT:
                    // put them at the left edge and stop their lateral momentum
                    mover->setLocation(Point3D((*blocksi)->getBounds().x-mover->getBounds().w,mover->getLocation().y,mover->getLocation().z));
                    mover->setMomentum(Point3D(0,mover->getMomentum().y,mover->getMomentum().z));
                    break;

                // smacked into the right edge of the block
                case COLLISION_RIGHT:
                    // stop them at the right edge and stop their lateral momentum
                    mover->setLocation(Point3D((*blocksi)->getBounds().x+(*blocksi)->getBounds().w,mover->getLocation().y,mover->getLocation().z));
                    /// KLUDGE: Only reset momentum if it is moving toward the block.
                    if (mover->getMomentum().x < 0 )
                        mover->setMomentum(Point3D(0,mover->getMomentum().y,mover->getMomentum().z));
                    break;

                // No collision? Don't do anything.
                default:
                    break;
            } // switch (col)

        } // if(collision occurred)

    } // for (each block)

}

/// TODO: This function appears to be broken. Fix plz kthanks
bool Physics::ApplyFriction(Actor* mover, Uint32 TimeDelta) {

    // if the player is on the ground, apply friction
    if ( mover->isOnGround() ) {

        // amount of friction applied, in pixels per second per second
        const int FRICTION_STRENGTH = 400;

        // adjust friction effect for the amount of time that has passed
        float friction = FRICTION_STRENGTH * TimeDelta / float(TICKS_PER_SECOND);

        // determine if current momentum is left-going or right-going
        float xmomentum = mover->getMomentum().x;

        // if object is going extremely slow in either direction
        if (xmomentum <= friction && xmomentum >= -friction) {
            // stop the object
            mover->setMomentum( Point3D(0,mover->getMomentum().y,mover->getMomentum().z) );
        }
        // if object is traveling right
        else if (xmomentum > friction) {
            // slow it's right-going movement
            mover->setMomentum( mover->getMomentum() + Point3D(-friction,0,0) );
        }
        // if object is moving left
        else if (xmomentum < -friction ) {
            mover->setMomentum( mover->getMomentum() + Point3D(friction,0,0) );
        }

    }

    return true;
}

bool Physics::ApplyGravity(Actor* mover, Uint32 TimeDelta) {

    // amount of gravity to apply (pixels per second per second)
    const int GRAVITY_STRENGTH = 2000;

    // maximum speed of falling
    const int MAX_FALL_SPEED   = 500;

    // adjust gravity strength for the amount of time that has passed
    float gravity = GRAVITY_STRENGTH * TimeDelta / float(TICKS_PER_SECOND);

    // adjust momentum for gravity
    mover->setMomentum( mover->getMomentum() + Point3D(0,gravity,0) );

    // don't let the object fall faster than the max fall speed
    if (mover->getMomentum().y > MAX_FALL_SPEED) {
        mover->setMomentum(
            Point3D(
            mover->getMomentum().x,
            MAX_FALL_SPEED,
            mover->getMomentum().z)
        );
    }

    mover->setOnGround(false);

    return true;

}

bool Physics::MoveObject(GameObject* mover, Uint32 TimeDelta) {

    // assign the object's previous location as the current location (before moving it)
    mover->setPrevLocation( mover->getLocation() );

    // create Point3D to hold adjusted position difference
    Point3D distance;

    // adjust momentum based on amount of time passed
    // * momentum is saved as pixels per second
    // * convert these values to the amounts since last frame
    distance.x = mover->getMomentum().x * TimeDelta / float(TICKS_PER_SECOND);
    distance.y = mover->getMomentum().y * TimeDelta / float(TICKS_PER_SECOND);
    distance.z = mover->getMomentum().z * TimeDelta / float(TICKS_PER_SECOND);

    // apply adjusted momentum
    mover->setLocation( mover->getLocation() + distance );

    return true;

}
