/* Window
 *
 * A class for creating and managing the GUI window.
 *
 * Not much to say here. It basically creates a window and can set the
 * title of it.
 */

#ifndef WINDOW_H
#define WINDOW_H

#include <string>
#include <SDL/SDL.h>

class Window {

	private:
		int m_iWidth;
		int m_iHeight;
		int m_iBPP;
		Uint32 m_iFlags;
		SDL_Surface* mWindow;
		std::string mTitle;

	public:
		Window();
		~Window();

		int GetWidth() const;
		int GetHeight() const;
		int GetBPP() const;
		Uint32 GetFlags() const;
		SDL_Surface* GetSurface() const;
		std::string GetTitle() const;

		void SetTitle(std::string title);
		bool Init(int width,int height,int bpp, Uint32 flags);
		bool Resize(int width, int height);

};

#endif
