/* Resources
 * A struct of static members that holds the various images and sounds used in
 * the game. It loads them all up at the start and unloads them all when finished.
 *
 * The initial design does not allow for dynamically choosing parts to load and use.
 * This would be very helpful and would save valuable memory on large projects, so
 * I hope to implement a new design like that at some point.
 */

/* Should resources specifically for the game be placed in the game's folder?
 * ...Should this be a part of Kaiken at all?
 * WHO AM I!?!
 */


#ifndef RESOURCES_H
#define RESOURCES_H

    // insert classes/defines here

    // fonts
    #define TestFont "./Graphics/demo_font.ttf"

    // sprite sheets
    #define SpaceEnemy "./Graphics/demo_enemy.png"
    #define SpacePlayer "./Graphics/demo_ship.png"
    #define SpaceBullet "./Graphics/demo_bullets.png"
    #define DemoGuy "./Graphics/demo_guy.png"
    #define Dirt "./Graphics/demo_dirt.png"
    #define Brick "./Graphics/demo_brick.png"
    #define Goal  "./Graphics/demo_goal.png"

    // backgrounds
    #define TitleBG "./Graphics/demo_titlescreen.png"
    #define SpaceBG "./Graphics/demo_space.png"
    #define SkyBG   "./Graphics/demo_sky.png"

    // Music
    #define TitleMusic "./Audio/title.ogg"

    // Sound effects

#endif
