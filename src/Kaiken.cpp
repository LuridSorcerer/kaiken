#include "Kaiken.h"
#include <iostream>
#include <sstream>
#include <fstream>

// Levels
//TitleScreen titlescreen;

/* Constructor
 * Initializes the variables. That's pretty much it.
 */
Kaiken::Kaiken() {

	// standard constructor to initialize variables

	// just started, so we aren't done yet
	m_bDone = false;

	// how many frames have been drawn?
	m_iFrameCount = 0;

	// is the music/sound enabled?
	//m_bSoundEnabled = true;
	m_bSoundEnabled = false;

	// get current time
	m_iTime = SDL_GetTicks();

}

/* Destructor
 * Deallocate any dynamically allocated memory43w32e
 */
Kaiken::~Kaiken() { }

/* Init
 * Initialize the application's objects and data, get ready to start
 * the program.
 */
bool Kaiken::Init() {

	// initialize SDL, end application if it fails
	if(SDL_Init(SDL_INIT_EVERYTHING) != 0) {
		return false;
	}

    // check if we should open it in fullscreen or not
    uint32_t screenMode = 0;
    int fullscreen = 0;

    std::ifstream cfg("fullscreen.txt");
    cfg >> fullscreen;

	// create the window, end application if it fails
	if (fullscreen) {
	    screenMode = SDL_FULLSCREEN;
    } else {
        screenMode = SDL_HWSURFACE | SDL_DOUBLEBUF;
    }

	if (mWindow.Init(ScreenWidth,ScreenHeight,32,screenMode) != true) {
		return false;
	}
	SDL_ShowCursor(SDL_DISABLE);

	//mWindow.SetTitle("Kaiken: To underwhelm is our specialty");
	mWindow.SetTitle("Kaiken: Keeping your expectations low since 2012");

	// create the sound system, disable sound if it fails
	if (mSound.Init(44100,AUDIO_S16SYS,2,1024) != true) {
		std::cerr << "Sound system failed to initialize\n";
		m_bSoundEnabled = false;
	}

	// create graphics system, end application if it fails
	if (mGraphics.Init(ScreenWidth,ScreenHeight,mWindow.GetSurface()) != true) {
		return false;
	}
	mGraphics.LoadFont(TestFont);

	// create random number generator
	mRandom.Init();

    // add the levels to the vector of levels
    TitleScreen* titlescreen = new TitleScreen();
    levels.push_back(titlescreen);

    PlatformLevel* platlvl = new PlatformLevel();
    levels.push_back(platlvl);

    SpaceLevel* spacelvl = new SpaceLevel();
    levels.push_back(spacelvl);

    // Initialize all the levels
    /* FIXME
     * What I'd LIKE to do is perform this in the Application::Update() function. This way,
     * levels wouldn't be Init()-ed until they are the next to run. Doing this here makes
     * every level that is in the game load up entirely into memory, even before they are needed.
     * For some reason I can't explain, referencing the levels inside the vector works here, but not
     * in the Update() function. If called there, only the first level will be given valid pointers to
     * the Graphics and Sound objects, causing a segfault when they try to draw to the screen or play
     * sounds.
     */
    for (uint32_t i = 0; i < levels.size(); i++) {
        levels[i]->Init(&mGraphics, &mSound, &ctrls);
    }

    // start at the first level
    currentLevel = 0;

	// success, return true
	return true;

}

void Kaiken::Run() {

	// implements our game loop

	// stores an event that we will read
	SDL_Event event;

	while(!m_bDone) {

		TakeInput(&event); // gather player input
		Update(); 	// update application data (process input, etc.)
		Render();	// render everything to background
	}

}

void Kaiken::TakeInput(SDL_Event* pEvent) {

	// read input here

	// SDL_PollEvent: asks the system if there are any events.
	// It will not pause and wait for events if there are none.
	while(SDL_PollEvent(pEvent)) {

		// read the event. OnEvent will pass the event pointer to the
		// correct input handling function
		OnEvent(pEvent);

	}
}

void Kaiken::Update() {
	// update game state of the current level

	// if we're not past all the levels
    if (currentLevel < levels.size() ) {

        // make sure level is initialized
        /* FIXME
         * Referencing the functions of a level from inside the vector this way
         * does not work. This is explained in more detail in the Kaiken::Init() funciton.
         */
        //levels[currentLevel]->Init(&mGraphics,&mSound,&ctrls);

        // Get new time
        Uint32 now = SDL_GetTicks();

        // Get time since last time check
        Uint32 TimeDelta = now - m_iTime;

        // Save new current time
        m_iTime = SDL_GetTicks();

        // update the level's state
        if (levels[currentLevel]->Update(TimeDelta)) {

            // if the level is done, go to the next
            currentLevel++;

        }

    // if we've got no more levels
    } else {

        // get ready to quit
        m_bDone = true;

        // go back to first level
        //currentLevel = 0;

    }

	// delay a bit
	SDL_Delay(12);


}

void Kaiken::Render() {

    // KLUDGE: Prevent trying to render a level that doesn't exist after the
    // last level has been completed.
    if ( currentLevel < levels.size() ) {
        levels[currentLevel]->Render();
    }

    // Things can be rendered to the screen independent of the current level being run
    // by calling the mGraphics object from here.

    // DEBUG:
    // Show what level we are on in the bottom left
    std::stringstream levelNumStr;
    levelNumStr << "Level: " << currentLevel;
    mGraphics.BlitText(levelNumStr.str(),0,450);

    // DEBUG:
    // Show mouse position for no particular reason
    //    std::stringstream mousePosStr;
    //    mousePosStr << "Mouse: " << ctrls.getMousePosition().x << "," << ctrls.getMousePosition().y;
    //    mGraphics.BlitText(mousePosStr.str(),0,450);

    // tell the Graphics object to render what has been drawn to the
    // backbuffer.
    mGraphics.Render();

}

void Kaiken::Quit() {
	// clean up, prepare to quit
	mSound.Quit(); // kill the sound system
	SDL_Quit();    // kill the SDL systems
}

void Kaiken::OnResize(int width, int height) {

	if(!(mWindow.Resize(width,height))) {
		OnExit();
	}

}

void Kaiken::OnExit() {
	m_bDone = true;
}

void Kaiken::OnKeyPress(SDLKey key, SDLMod mod, Uint16 unicode) {

	switch (key) {
		// if space is pressed, play a sound
		case SDLK_SPACE:
			ctrls.setButton(BUTTON_SHOOT, true);
			break;

		case SDLK_RETURN:
			ctrls.setButton(BUTTON_START, true);
			break;

		// if left is pressed, move character left
		case SDLK_LEFT:
			ctrls.setButton(BUTTON_LEFT, true);
			break;

		// if right is pressed, move character right
		case SDLK_RIGHT:
			ctrls.setButton(BUTTON_RIGHT, true);
			break;

        case SDLK_UP:
            ctrls.setButton(BUTTON_UP, true);
            break;

        case SDLK_DOWN:
            ctrls.setButton(BUTTON_DOWN, true);

        case SDLK_m:
            //m_bMouseCtl = !m_bMouseCtl;
            break;

        case SDLK_BACKQUOTE:
            ctrls.setButton(BUTTON_DEBUG, true);
            break;

		// if escape is pressed, quit
		case SDLK_ESCAPE:
			m_bDone = true;
			break;

        default:
            break;
		}

}

void Kaiken::OnKeyRelease(SDLKey key, SDLMod mod, Uint16 unicode) {

	switch (key) {

		case SDLK_SPACE:
            ctrls.setButton(BUTTON_SHOOT, false);
            break;

		case SDLK_RETURN:
			ctrls.setButton(BUTTON_START, false);
			break;

		// stop moving left when left button is released
		case SDLK_LEFT:
			ctrls.setButton(BUTTON_LEFT, false);
			break;

		// stop moving right when right is released
		case SDLK_RIGHT:
			ctrls.setButton(BUTTON_RIGHT, false);
			break;

        case SDLK_UP:
            ctrls.setButton(BUTTON_UP, false);

        case SDLK_DOWN:
            ctrls.setButton(BUTTON_DOWN, false);

        case SDLK_BACKQUOTE:
            ctrls.setButton(BUTTON_DEBUG, false);
            break;

        default:
            break;

		}

}

void Kaiken::OnMouseMotion(int mX, int mY, int relX, int relY, bool left, bool middle, bool right) {
    // move the ship according to mouse location
    ctrls.setMousePosition( mX, mY);

}

void Kaiken::OnMousePress(Uint8 button, int x, int y) {
    // if left button is pressed, shoot
    if (button == SDL_BUTTON_LEFT)
        ctrls.setButton(BUTTON_SHOOT, true);
}

void Kaiken::OnMouseRelease(Uint8 button, int x, int y) {
    // if left button is released, allow for shooting
    if (button == SDL_BUTTON_LEFT)
        ctrls.setButton(BUTTON_SHOOT, false);
}
