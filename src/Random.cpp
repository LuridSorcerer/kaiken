#include "Random.h"

Random::Random() { }

Random::~Random() { }

bool Random::Init() {
	// seed random number
	srand(time(NULL));
	
	// success, return true
	return true;
}

int Random::GetRandomInt(int min, int max) {
	
	if (max < min) return 0;
	
	int num;
	
	num = rand() % (max - min);
	num += min;
	
	return num;
	
}
