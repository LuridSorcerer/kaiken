#include "Graphics.h"
#include <iostream>
#include <sstream>

/* ========================================================== */
/* ======================== Graphics ======================== */
/* ========================================================== */

Graphics::Graphics() {
	// standard constructor, init variables
	m_iWidth = 0;
	m_iHeight = 0;
	mScreen = NULL;
	mBG = NULL;
}

Graphics::~Graphics() {
	// destructor, free any dynamically allocated memory
}

bool Graphics::Init(int width, int height, SDL_Surface* screen) {

	// save the passed-in variables
	m_iWidth = width;
	m_iHeight = height;
	mScreen = screen;
	mFont = NULL;

	// return false if the screen was invalid
	if(!mScreen) {
		return false;
	}

	// initialize TrueType Fonts
	if (TTF_Init() == -1) {
		std::cerr << "couldn't load TTF support\n";
		return false;
	}

	// success, return true
	return true;

}

bool Graphics::LoadBackground(std::string filename) {

	// load up a background file
	SDL_Surface* temp = IMG_Load( filename.c_str() );
	if(!temp) {
		return false;
	}

	// optimize image
	mBG = SDL_DisplayFormat(temp);

	// free temporary surface
	SDL_FreeSurface(temp);

	return true;

}

SDL_Surface* Graphics::LoadSpritesheet(std::string filename) {

	// load up the image
	SDL_Surface* temp = IMG_Load( filename.c_str() );
	if(!temp) {
		return NULL;
	}

	// optimize image, keep transparency information
	SDL_Surface *returnimage = SDL_DisplayFormatAlpha(temp);

	// free the temp surface
	SDL_FreeSurface(temp);

	// return optimized image
	return returnimage;
}

bool Graphics::SetBackground(SDL_Surface* bg) {

	// save the pointer to the background image
	mBG = bg;

	return mBG;

}

bool Graphics::RenderBackground() {

	// blit everything to the screen surface

	// background
	if (mBG != NULL) {
		SDL_BlitSurface(mBG,NULL,mScreen,NULL);
	}

	// switch buffers to show new image
	//SDL_Flip(mScreen);

    return true;

}

bool Graphics::RenderSprite(Sprite renderme) {

	// create the "rectangle" we're drawing to
	SDL_Rect destrect;
	destrect.x = renderme.x;
	destrect.y = renderme.y;

	// draw the sprite on the screen
	SDL_BlitSurface(renderme.sheet,renderme.clip,mScreen,&destrect);

	return true;
}

bool Graphics::RenderSprite(Sprite renderme, Point3D where) {

	// create the "rectangle" we're drawing to
	SDL_Rect destrect;
	destrect.x = int(where.x);
	destrect.y = int(where.y);

	// draw the sprite on the screen
	SDL_BlitSurface(renderme.sheet,renderme.clip,mScreen,&destrect);

	return true;
}

bool Graphics::LoadFont(std::string filename) {
	// load font from file, give it a size
	mFont = TTF_OpenFont(filename.c_str(), 16);

	// if loading failed, return false
	if (!mFont) {

		return false;
	}

	// success! return true
	return true;
}

bool Graphics::BlitText(std::string text, int x, int y) {

	// create the surface with the message
	SDL_Color fontColor =  {255,255,255};
	SDL_Surface* message;
	message = TTF_RenderText_Solid(mFont, text.c_str(), fontColor);

	// blit the message to the screen
	SDL_Rect destRect;
	destRect.x = x;
	destRect.y = y;
	SDL_BlitSurface(message,NULL,mScreen,&destRect);

	// clean up the message surface
	SDL_FreeSurface(message);

	return true;

}

bool Graphics::Render() {
	// switch buffers to show new image
	SDL_Flip(mScreen);
	return true;
}

void Graphics::Quit() {

	// free the background
	if (mBG) {
		SDL_FreeSurface(mBG);
		mBG = NULL;
	}

}
