/* Input core
 *
 * Basic Interface class used for collecting user input via mice and
 * keyboards.
 *
 * TODO: game controllers?
 */

#ifndef INPUTCORE_H
#define INPUTCORE_H

#include <SDL/SDL.h>

class InputCore {

	public:
		InputCore() { }
		virtual ~InputCore() { }

		// event handler, calls other event functions
		void OnEvent(SDL_Event* pEvent);

		// key events
		virtual void OnKeyPress(SDLKey key, SDLMod mod, Uint16 unicode) { }
		virtual void OnKeyRelease(SDLKey key, SDLMod mod, Uint16 unicode) { }

		// mouse events
		virtual void OnMouseMotion(int mX, int mY, int relX, int relY,
			bool left, bool middle, bool right) { }
		virtual void OnMousePress(Uint8 button, int x, int y) { }
		virtual void OnMouseRelease(Uint8 button, int x, int y) { }

		// window events
		virtual void OnFocusChange(Uint8 state, Uint8 gain) { }
		virtual void OnResize(int width, int height) { }
		virtual void OnExit() { }

};

#endif
