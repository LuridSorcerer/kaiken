#include "Point3D.h"

/* ========================================================== */
/* ======================== Point3D ========================= */
/* ========================================================== */

Point3D Point3D::operator+ (const Point3D& addend) const {
    Point3D sum;
    sum.x = x + addend.x;
    sum.y = y + addend.y;
    sum.z = z + addend.z;
    return sum;
}

Point3D Point3D::operator- (const Point3D& addend) const {
    Point3D difference;
    difference.x = x - addend.x;
    difference.y = y - addend.y;
    difference.z = z - addend.z;
    return difference;
}
