/* GameObject:
    Game objects are objects that appear in the game. I KNOW! Ingenious,
    right?! They can be blocks, enemy players, or even the player's character.
    It is basically an interface; there are no GameObject objects, only
    objects of classes that inherit from GameObject.
*/

#include "GameObject.h"
#include <iostream>

/* ========================================================== */
/* ======================= GameObject ======================= */
/* ========================================================== */

SDL_Rect GameObject::getBounds() {

    // get the location
    bounds.x = Sint16(location.x);
    bounds.y = Sint16(location.y);

    // get height and width
    if (sprite.clip != NULL) {
        // if a blitting clip is given, use that height and width
        bounds.h = Uint16(sprite.clip->h);
        bounds.w = Uint16(sprite.clip->w);
    } else {
        // if there is no blitting clip defined, use the height
        // and width of the spritesheet
        bounds.h = Uint16(sprite.sheet->h);
        bounds.w = Uint16(sprite.sheet->w);
    }

    // return rectangle
    return bounds;

}

Point3D  GameObject::getLocation() { return location; }

Point3D  GameObject::getPrevLocation() { return prevLocation; }

Point3D  GameObject::getMomentum() { return momentum; }

Sprite   GameObject::getSprite() { return sprite; }

bool     GameObject::getVisible() { return visible; }

//void setBounds(); // doubt I'll need this.

void GameObject::setLocation(Point3D newLocation) { location = newLocation; }

void GameObject::setPrevLocation(Point3D newLocation) { prevLocation = newLocation; }

void GameObject::setMomentum(Point3D newMomentum) { momentum = newMomentum; }

//void setSprite(Sprite);

void GameObject::setVisible(bool newVisible){ visible = newVisible; }

/* ========================================================== */
/* ========================== Block ========================= */
/* ========================================================== */

bool Block::Init(Sprite newSprite, Point3D newLocation) {

    SDL_Rect* newBounds = new SDL_Rect();
    bounds = *newBounds;

    sprite = newSprite;
    location = newLocation;
    prevLocation = newLocation;
    return true;
}

/* ========================================================== */
/* ========================== Actor ========================= */
/* ========================================================== */

bool Actor::Init(Sprite newSprite, Point3D newLocation) {

    sprite = newSprite;
    location = newLocation;
    prevLocation = newLocation;
    //previousLocation = newLocation; // where this object was
                                    // after the last update.
    alive = true;
    onGround = false;

    return true;

}

bool Actor::isAlive() { return alive;}

bool Actor::isOnGround() { return onGround; }

void Actor::setAlive(bool isAlive) { alive = isAlive; }

void Actor::setOnGround(bool isOnGround) { onGround = isOnGround; }

void Actor::update( ) { }

/* ========================================================== */
/* ========================= CPUActor ======================= */
/* ========================================================== */

bool CPUActor::Init(Sprite newSprite, Point3D newLocation) {

    sprite = newSprite;
    location = newLocation;
    prevLocation = newLocation;
    alive = true;

    return true;
}

void CPUActor::tickAI() {

}

/* ========================================================== */
/* ========================== CPUShip ======================= */
/* ========================================================== */

void CPUShip::tickAI() {

    if ( momentum.x == 0 ) {
        momentum.x = 1;
    }

    // turn around at the right edge
    if ( momentum.x > 0 && location.x > 500 ) {
        momentum.x = momentum.x * -1;
    }

    // turn around at the left edge
    if (momentum.x < 0 && location.x < 1 ) {
        momentum.x = momentum.x * -1;
    }

}

/* ========================================================== */
/* ========================== Bullet ======================== */
/* ========================================================== */

/* bool Bullet::Init(Sprite mySprite,Point3D myLocation,
                  Point3D myMomentum,GameObject* myOwner) {
    sprite = mySprite;
    location = myLocation;
    prevLocation = myLocation;
    momentum = myMomentum;
    owner = myOwner;
    alive = true;

    return true;
}

GameObject* Bullet::getOwner() { return owner; }

bool Bullet::setOwner (GameObject* myOwner) { owner = myOwner; return true; }
*/
