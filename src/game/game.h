/* game.h
    All of the include files for the game all in one convenient location.
*/

/* Vague and unhelpful Changelog
 *
 *  2012-12-28:
 *      Created all-new "artwork" (ship,enemy ship, space background, title background, platforming character)
 *      Separated Kaiken and game code
 *      Fully animated player ships, enemy ships, and...some guy.
 *      TitleScreen: now using list<CPUActors*> instead of list<CPUActors>
 *          (This change will eventually be applied in Kaiken's Level base class)
 *      More crap added to TitleScreen
 *          Player ship, bunch of enemy ships in random places moving randomly
 *          Enemy ships are properly deleted when they touch the edge of the screen
 *      Added second "level", Space Shooter
 *          Bullets, moving and dying enemy ships
 *      Fixed a great many bugs I introduced or revealed (mostly introduced)
 *
 */

#ifndef GAME_H
#define GAME_H

    // GameActor types
    #include "EnemyShip.h"
    #include "PlayerShip.h"
    #include "Guy.h"
    #include "Bullet.h"

    // Level types
    #include "TitleScreen.h"
    #include "SpaceLevel.h"
    #include "PlatformLevel.h"

#endif
