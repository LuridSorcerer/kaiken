/*
    EnemyShip
    The AI-controlled enemy ships for space shooter levels
*/

#ifndef ENEMYSHIP_H
#define ENEMYSHIP_H

#include "../Kaiken.h"

extern SDL_Rect ESHIP_1;
extern SDL_Rect ESHIP_2;
extern SDL_Rect ESHIP_3;
extern SDL_Rect ESHIP_4;

extern SDL_Rect ESHIP_DEAD_1;
extern SDL_Rect ESHIP_DEAD_2;
extern SDL_Rect ESHIP_DEAD_3;
extern SDL_Rect ESHIP_DEAD_4;

class EnemyShip : public CPUActor {

    protected:
    uint32_t age;
    bool gone;
    int goneDelay;

    public:
    EnemyShip() {}

    bool Init(Sprite,Point3D);
    void update();

    void setGone(bool newGone) { gone = newGone; }
    bool isGone() {return gone;}

};

#endif
