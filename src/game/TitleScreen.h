/* TitleScreen.h
    Defines a type of Level that displays a title screen. And nothing else.
    It quits when the player presses Start or Enter.
*/

#ifndef TITLESCREEN_H
#define TITLESCREEN_H

#include "../Kaiken.h"
#include "game.h"

class PlayerShip;
class Guy;

class TitleScreen : public Level {

    protected:

    // list of Actors (AI enemies,etc.)
    std::list <CPUActor*> pCpuActors;

    // TODO: Probably want to add a list of Bullets.
    // I made the Bullet class, right?...

    // player
    //Actor player;
    PlayerShip* pship;
    Guy* guy;

    public:
    TitleScreen(); // A constructor should be added that reads from a file. this will be
             // useful once a level file format has been created.
    ~TitleScreen();
    virtual bool Init(Graphics*,Sound*,Controls*); // returns true if initialized successfully,
             // returns false if wasn't required. For hard-coded levels, the blocks and
             // actors will be defined and added to their respective lists here.
    virtual bool Update(Uint32); // returns whether or not the level is done.
    virtual bool Render(); // returns true. May be used in the future.

};


#endif
