/*
    PlatformLevel:
    Generic level in which the player runs and jumps on stuff.
*/

#include "PlatformLevel.h"
#include <sstream>

PlatformLevel::PlatformLevel() {
    // initialize the pointers
    m_pGraphics = NULL;
    m_pSound = NULL;
    m_pControls = NULL;
    m_bInitialized = false;
}

PlatformLevel::~PlatformLevel() {}

bool PlatformLevel::Init(Graphics* gptr, Sound* sptr, Controls* cptr) {

    if (!m_bInitialized) {

        // save pointers
        m_pGraphics = gptr;
        m_pSound = sptr;
        m_pControls = cptr;

        // load level stuff

        // load sky
        sky_sheet = m_pGraphics->LoadSpritesheet(SkyBG);
        background = sky_sheet;

        // load player
        canJump = false;
        guy_sheet = m_pGraphics->LoadSpritesheet(DemoGuy);
        guy = new Guy();
        guy->Init(Sprite(guy_sheet,NULL),Point3D(50,300,0));

        // load blocks
        dirt_sheet = m_pGraphics->LoadSpritesheet(Dirt);
        brick_sheet = m_pGraphics->LoadSpritesheet(Brick);

        // bottom layer of dirt
        for (int i = 0; i < 200; i++) {
            SDL_Rect* bottomdirt = new SDL_Rect();
            bottomdirt->h=32; bottomdirt->w=32; bottomdirt->x=0; bottomdirt->y=0;
            Block* new_block = new Block();
            new_block->Init( Sprite(dirt_sheet,bottomdirt),Point3D(i*bottomdirt->w,ScreenHeight-bottomdirt->h,0) );
            blocks.push_back(new_block);
        }

        // top layer of dirt
        for (int i = 0; i < 200; i++) {
            SDL_Rect* bottomdirt = new SDL_Rect();
            bottomdirt->h=32; bottomdirt->w=32; bottomdirt->x=32; bottomdirt->y=0;
            Block* new_block = new Block();
            new_block->Init( Sprite(dirt_sheet,bottomdirt),Point3D(i*bottomdirt->w,ScreenHeight-bottomdirt->h*2,0) );
            blocks.push_back(new_block);
        }

        // some blocks
        for (int i = 0; i < 10; i++) {
            Block* new_block = new Block();
            new_block->Init( Sprite(brick_sheet,NULL),Point3D(i*32+200,300,0) );
            blocks.push_back(new_block);
        }
        {
            Block* new_block = new Block();
            new_block->Init( Sprite(brick_sheet,NULL),Point3D(520,350,0) );
            blocks.push_back(new_block);
        }
        {
            Block* new_block = new Block();
            new_block->Init( Sprite(brick_sheet,NULL), Point3D(700,ScreenHeight-96,0) );
            blocks.push_back(new_block);
        }


        // create goal
        SDL_Surface* goal_sheet;
        goal_sheet = m_pGraphics->LoadSpritesheet(Goal);

        goal = new Block();
        goal->Init( Sprite(goal_sheet,NULL), Point3D(1500,ScreenHeight-64-128,0) ); ///KLUDGE: magic numbers

        // create camera
        camera = new Block();
        camera->Init( Sprite(sky_sheet,NULL), Point3D(0,0,0) );
        camera->getBounds();

        // now initialized
        m_bInitialized = true;
        return true;

    } else { return false; }

}

bool PlatformLevel::Update(Uint32 TimeDelta) {

    // move player

    // Move left if left is pressed
    if (m_pControls->getButton(BUTTON_LEFT)) {
        guy->setMomentum( guy->getMomentum() - Point3D(5,0,0) );

        // restrict to maximum speed
        if (guy->getMomentum().x < -MAXSPEED) {
            guy->setMomentum(Point3D(-MAXSPEED,guy->getMomentum().y,guy->getMomentum().z));
        }

    // move right if right is pressed
    } else if (m_pControls->getButton(BUTTON_RIGHT)) {
        guy->setMomentum( guy->getMomentum() + Point3D(5,0,0) );

        // restrict to maximum speed
        if (guy->getMomentum().x > MAXSPEED) {
            guy->setMomentum(Point3D(MAXSPEED,guy->getMomentum().y,guy->getMomentum().z));
        }
    }

    // stop dead if player presses down
    if (m_pControls->getButton(BUTTON_DOWN) ) {
        guy->setMomentum( Point3D(0.0,guy->getMomentum().y,guy->getMomentum().z) );
    }

    // if player can jump and is pressing jump button...
    if ( (canJump>0) && m_pControls->getButton(BUTTON_SHOOT) ) {

        // jump
        guy->setMomentum(Point3D( guy->getMomentum().x, -JUMPPOWER, guy->getMomentum().z ));

        // decrease remaining jump power
        canJump--;
    }

    // if player is already in the air and not pressing jump button...
    if ( guy->isOnGround()==false && !m_pControls->getButton(BUTTON_SHOOT) ) {

        // take away any remaining jump power
        canJump=0;
    }

    // apply momentum
    Physics::MoveObject(guy,TimeDelta);

    // update player (sprite, etc.)
    guy->update();

    // move AI actors

    // check physics stuff

    // apply gravity
    Physics::ApplyGravity(guy,TimeDelta);

    // apply friction
    /// Broken?
    if (!m_pControls->getButton(BUTTON_RIGHT) && !m_pControls->getButton(BUTTON_LEFT)) {
        Physics::ApplyFriction(guy,TimeDelta);
    }

    // check collisions with blocks
    Physics::CheckBlockCollisions(guy,blocks);

    /// if they've hit the top of a block, the rest of their jump is spent

    // if they've landed on a block, they may jump again
    if(guy->isOnGround() && !m_pControls->getButton(BUTTON_SHOOT)) { canJump = JUMPLENGTH; }

    // move enemies

    // move enemies

    // reorient camera
    camera->setLocation( Point3D(guy->getLocation().x-(ScreenWidth/2),guy->getLocation().y-(ScreenHeight/2),0) );
    if (camera->getLocation().x > CAM_X_MAX) { camera->setLocation( Point3D(CAM_X_MAX,camera->getLocation().y,camera->getLocation().z) ); }
    if (camera->getLocation().x < CAM_X_MIN) { camera->setLocation( Point3D(CAM_X_MIN,camera->getLocation().y,camera->getLocation().z) ); }
    if (camera->getLocation().y > CAM_Y_MAX) { camera->setLocation( Point3D(camera->getLocation().x,CAM_Y_MAX,camera->getLocation().z) ); }
    if (camera->getLocation().y < CAM_Y_MIN) { camera->setLocation( Point3D(camera->getLocation().x,CAM_Y_MIN,camera->getLocation().z) ); }

    // check goal
    if ( Physics::CheckBoxCollision(guy,goal) != COLLISION_NONE ) {
        return true;
    }

    // if (complete)
    //return true;

    // not done?
    return false;

}

bool PlatformLevel::Render() {

    // draw background
    m_pGraphics->SetBackground(background);
    m_pGraphics->RenderBackground();

    // draw blocks
    int blockcount = 0;
    std::list<Block*>::iterator blocksi;
    for(blocksi=blocks.begin();blocksi!=blocks.end();blocksi++){
        if ( Physics::CheckBoxCollision( camera, (*blocksi) ) != COLLISION_NONE ) {
            m_pGraphics->RenderSprite( (*blocksi)->getSprite(), (*blocksi)->getLocation()-camera->getLocation() );
            blockcount++;
        }
    }

    // draw goal
    m_pGraphics->RenderSprite(goal->getSprite(),goal->getLocation()-camera->getLocation());

    // draw AI actors

    // draw player
    //Point3D junk = camera->getLocation();
    m_pGraphics->RenderSprite(guy->getSprite(), guy->getLocation()-camera->getLocation() );

    // debug info
    if (m_pControls->getButton(BUTTON_DEBUG)) {
        std::stringstream debugstr;
        debugstr << "Camera: " << camera->getLocation().x << "x" << camera->getLocation().y;
        m_pGraphics->BlitText(debugstr.str(),0,0);
        debugstr.str("");
        debugstr << "Blocks drawn: " << blockcount;
        m_pGraphics->BlitText(debugstr.str(),0,32);
    }
    // done
    return true;
}
