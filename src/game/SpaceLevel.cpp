/* SpaceLevel
    A shameless and watered-down ripoff of Space Invaders.
    Just a basic Level.
*/

#include "SpaceLevel.h"

SpaceLevel::SpaceLevel() {

    // initialize the pointers
    m_pGraphics = NULL;
    m_pSound = NULL;
    m_pControls = NULL;
    m_bInitialized = false;

}

SpaceLevel::~SpaceLevel() {

    // delete the player
//    delete pship;
//    pship = NULL;

    // delete the enemy ships
//    std::list<EnemyShip*>::iterator actorsi;
//    for(actorsi=eships.begin();actorsi!=eships.end();actorsi++) {
//        delete (*actorsi);
//        (*actorsi) = NULL;
//    }
}

bool SpaceLevel::Init(Graphics* gptr, Sound* sptr, Controls* cptr) {

    // if not already initialized
    if (!m_bInitialized) {

        // save the pointers
        m_pGraphics = gptr;
        m_pSound = sptr;
        m_pControls = cptr;

        // load background
        background = m_pGraphics->LoadSpritesheet( SpaceBG );

        // create the player
        pship = new PlayerShip();
        SDL_Surface* pship_sheet = m_pGraphics->LoadSpritesheet(SpacePlayer);
        pship->Init( Sprite(pship_sheet,NULL), Point3D(0,400,0) );

        // create enemies
        SDL_Surface* eship_sheet = m_pGraphics->LoadSpritesheet(SpaceEnemy);
        for (int i=0; i < 10; i++) {
            EnemyShip* eship = new EnemyShip();
            eship->Init( Sprite(eship_sheet,NULL), Point3D(Random::GetRandomInt(1,ScreenWidth),Random::GetRandomInt(0,ScreenHeight/2),0 ));
            //eship->setLocation( Point3D(500,100,0) );
            eships.push_back(eship);
        }

        // load bullet texture
        bullet_sheet = m_pGraphics->LoadSpritesheet(SpaceBullet);

        canShoot = true;

        // now initialized
        m_bInitialized = true;

        return true;
    } else {

        // already initialized, skip it.
        return false;
    }
}

bool SpaceLevel::Update(Uint32 TimeDelta) {

    // start music if it isn't started

    // move the ship
    pship->setLocation( Point3D(
        m_pControls->getMouseX()-(pship->getBounds().w/2),
        pship->getLocation().y,
        pship->getLocation().z) );
    // update the ship
    pship->update();

    // update the enemy ships
    std::list<EnemyShip*>::iterator eshipsi;
    for(eshipsi=eships.begin();eshipsi!=eships.end();eshipsi++) {
        Physics::MoveObject((*eshipsi),TimeDelta);
        (*eshipsi)->update();
        // if the ship is dead and gone, remove it
        if ((*eshipsi)->isGone()) {
            std::list<EnemyShip*>::iterator eraseme = eshipsi; eshipsi--;
            eships.erase(eraseme);
            continue;
        }
    }

    // update and move the bullets
    std::list<Bullet*>::iterator bulletsi;
    for(bulletsi=bullets.begin();bulletsi!=bullets.end();bulletsi++) {
        Physics::MoveObject((*bulletsi),TimeDelta);
        // if the bullet is off-screen, remove it
        if ((*bulletsi)->getLocation().y < -((*bulletsi)->getBounds().h)) {
            std::list<Bullet*>::iterator eraseme = bulletsi; bulletsi--;
            bullets.erase(eraseme);
            continue;
        }

    }

    // check for collisions between the bullets and the ships
    //std::list<Bullet*>::iterator bulletsi;
    for(bulletsi=bullets.begin();bulletsi!=bullets.end();bulletsi++) {
        //std::list<EnemyShip*>iterator eshipsi;
        for(eshipsi=eships.begin();eshipsi!=eships.end();eshipsi++) {
            // don't bother if the ship is dead
            if ((*eshipsi)->isAlive() != true) { continue; }

            // check for collision
            if (Physics::CheckCircleCollision((*bulletsi),(*eshipsi)) != COLLISION_NONE) {

                // kill the ship
                (*eshipsi)->setAlive(false);

                // delete the bullet
                std::list<Bullet*>::iterator eraseme = bulletsi; bulletsi--;
                bullets.erase(eraseme);

                // proceed to next bullet
                continue;
            }
        }
    }

    // create a bullet if the player shot
    if (canShoot && m_pControls->getButton(BUTTON_SHOOT)) {
        Bullet* bullet = new Bullet();
        bullet->Init(
            Sprite(bullet_sheet,&BULLET_UP),
            Point3D(pship->getLocation().x+(pship->getBounds().w/2)-(BULLET_UP.w/2),pship->getLocation().y,0),
            Point3D(0,-300,0),
            pship);
        bullets.push_back(bullet);
        canShoot = false;
    }

    if (!(m_pControls->getButton(BUTTON_SHOOT))) { canShoot = true; }

    return false;
}

bool SpaceLevel::Render() {

    // draw background
    m_pGraphics->SetBackground(background);
    m_pGraphics->RenderBackground();

    // draw the bullets
    std::list<Bullet*>::iterator bulletsi;
    for(bulletsi=bullets.begin();bulletsi!=bullets.end();bulletsi++) {
        m_pGraphics->RenderSprite( (*bulletsi)->getSprite(), (*bulletsi)->getLocation() );
    }

    // draw the enemy ships
    std::list<EnemyShip*>::iterator eshipsi;
    for(eshipsi=eships.begin();eshipsi!=eships.end();eshipsi++) {
        m_pGraphics->RenderSprite( (*eshipsi)->getSprite(), (*eshipsi)->getLocation() );
    }

    // draw the player's ship
    m_pGraphics->RenderSprite(pship->getSprite(),pship->getLocation());

    // done
    return true;

}
