/*
    PlatformLevel:
    Generic level in which the player runs and jumps on stuff.
*/

#ifndef PLATFORMLEVEL_H
#define PLATFORMLEVEL_H

#include "../Kaiken.h"
#include "game.h"

class PlatformLevel : public Level {

    // items needed for the level
    // most are already declared in Level class
    protected:

    static const int MAXSPEED=200;
    static const int JUMPPOWER=300;
    static const int JUMPLENGTH=20;

    static const int CAM_X_MIN=0;
    static const int CAM_X_MAX=1000;
    static const int CAM_Y_MIN=0;
    static const int CAM_Y_MAX=0;

    Block* goal;
    Block* camera; ///KLUDGE: All GameObjects have sprites, so making a spriteless object is infeasable at best
    std::list<Block*> blocks;
    Guy* guy;
    int canJump;
    SDL_Surface* sky_sheet;
    SDL_Surface* guy_sheet;
    SDL_Surface* dirt_sheet;
    SDL_Surface* brick_sheet;
    // max running speed?

    public:

    // construction/deconstructor
    PlatformLevel();
    ~PlatformLevel();

    // functions needed for running
    virtual bool Init(Graphics*,Sound*,Controls*);
    virtual bool Update(Uint32);
    virtual bool Render();

};

#endif // PLATFORMLEVEL_H
