/*
    EnemyShip
    The AI-controlled enemy ships for space shooter levels
*/

#include "EnemyShip.h"

SDL_Rect ESHIP_1 = {0,0,32,32};
SDL_Rect ESHIP_2 = {32,0,32,32};
SDL_Rect ESHIP_3 = {64,0,32,32};
SDL_Rect ESHIP_4 = {96,0,32,32};

SDL_Rect ESHIP_DEAD_1 = {0,32,32,32};
SDL_Rect ESHIP_DEAD_2 = {32,32,32,32};
SDL_Rect ESHIP_DEAD_3 = {64,32,32,32};
SDL_Rect ESHIP_DEAD_4 = {96,32,32,32};

bool EnemyShip::Init(Sprite mySprite, Point3D myLocation) {

    // brand new
    age = 0;

    // set sprite and location based on values passed in
    sprite=mySprite;
    location=myLocation;

    // give the ship random direction and speed
    momentum.x = 25 * Random::GetRandomInt(-10,10);

    // set starting animation frame
    sprite.clip = &ESHIP_1;

    // hopefully not starting out dead
    setAlive(true);
    setGone(false);
    goneDelay = 5;

    return true;
}

void EnemyShip::update() {

    CPUActor::update();

    // keep track of how long the ship has been alive
    // can be used for animations
    age++;


    ///FIXME: There is a bug in the getBounds() function!
    ///  It can sometimes return crap values, whereas the sprite clip data
    ///  (where it's supposed to be pulling data from) is consistently correct.
    // don't let the ship leave the screen
    if (location.x <= 0 ) {
        location.x = 0;
        if (momentum.x < 0) { momentum.x *= -1; }
    } else if (location.x > (ScreenWidth-sprite.clip->w))  {
        location.x = ScreenWidth-sprite.clip->w;
        momentum.x *= -1;
    }


    // idle animation
    if (age%8 == 0 && isAlive() ) {
        if(sprite.clip == &ESHIP_1) {
            sprite.clip = &ESHIP_2;
        } else if(sprite.clip == &ESHIP_2) {
            sprite.clip = &ESHIP_3;
        } else if (sprite.clip == &ESHIP_3) {
            sprite.clip = &ESHIP_4;
        } else if (sprite.clip == &ESHIP_4) {
            sprite.clip = &ESHIP_1;
        } else {
            sprite.clip = &ESHIP_1;
        }
    }

    // death animation
    if (age%8 == 0 && !isAlive()) {
        if(sprite.clip == &ESHIP_DEAD_1) {
            sprite.clip = &ESHIP_DEAD_2;
        } else if(sprite.clip == &ESHIP_DEAD_2) {
            sprite.clip = &ESHIP_DEAD_3;
        } else if(sprite.clip == &ESHIP_DEAD_3) {
            sprite.clip = &ESHIP_DEAD_4;
        } else if (sprite.clip == &ESHIP_DEAD_4) {
            // no change needed, stay this way
        } else {
            // must not be doing the dead animation yet
            // start it
            sprite.clip = &ESHIP_DEAD_1;
        }
        //stop the ship if it's dead
        momentum = Point3D(0,0,0);
        // decrement the timer for it to be removed
        goneDelay--;
        if (goneDelay<=0) { setGone(true); }

    }

}

