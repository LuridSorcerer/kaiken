/* Bullet
    Projectiles that can harm other actors.
*/

#ifndef BULLET_H
#define BULLET_H

#include "../Kaiken.h"

// sprite clips, defined in Bullet.cpp
extern SDL_Rect BULLET_UP;
extern SDL_Rect BULLET_RIGHT;
extern SDL_Rect BULLET_LEFT;
extern SDL_Rect BULLET_DOWN;

class Bullet : public Actor {

    protected:
    GameObject* owner;

    public:
    Bullet() {}
    ~Bullet() {}

    bool Init(Sprite,Point3D,Point3D,GameObject*);
    GameObject* getOwner() { return owner; }
    bool setOwner(GameObject* myOwner) { owner = myOwner; return true; }

};

#endif // BULLET_H
