/* Bullet
    Projectiles that can harm other actors.
*/

#include "Bullet.h"

// Bullet sprite clips
SDL_Rect BULLET_UP = {0,0,4,4};
SDL_Rect BULLET_RIGHT = {4,0,4,4};
SDL_Rect BULLET_LEFT = {8,0,4,4};
SDL_Rect BULLET_DOWN = {12,0,4,4};

bool Bullet::Init(Sprite mySprite,
                  Point3D myLocation,
                  Point3D myMomentum,
                  GameObject* myOwner) {
    sprite = mySprite;
    location = myLocation;
    momentum = myMomentum;
    owner = myOwner;

    return true;
}
