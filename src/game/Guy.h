/* Guy
 * Some guy we can use for platforming levels.
 */

#ifndef GUY_H
#define GUY_H

#include "../Kaiken.h"

// Sprite clips
extern SDL_Rect GUY_R_STAND;
extern SDL_Rect GUY_R_WALK1;
extern SDL_Rect GUY_R_WALK2;
extern SDL_Rect GUY_R_WALK3;
extern SDL_Rect GUY_R_JUMP;
extern SDL_Rect GUY_R_ATTACK;

extern SDL_Rect GUY_L_STAND;
extern SDL_Rect GUY_L_WALK1;
extern SDL_Rect GUY_L_WALK2;
extern SDL_Rect GUY_L_WALK3;
extern SDL_Rect GUY_L_JUMP;
extern SDL_Rect GUY_L_ATTACK;

// Facings
enum {
    FACING_LEFT,
    FACING_RIGHT
};

class Guy : public Actor {

    protected:
    uint32_t age;
    int facing;

    public:
    Guy() { }
    bool Init(Sprite,Point3D);
    void update();
//    SDL_Rect getBounds();
//    void setLocation(Point3D newLocation);


};

#endif
