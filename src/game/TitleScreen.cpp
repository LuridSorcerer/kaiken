/* TitleScreen.h
    Defines a type of Level that displays a title screen. And nothing else.
    It quits when the player presses Start or Enter.
*/

#include "TitleScreen.h"

TitleScreen::TitleScreen() {

    // init those member variables!
    m_pGraphics = NULL;
    m_pSound = NULL;
    m_pControls = NULL;

    m_bInitialized = false;

}

TitleScreen::~TitleScreen() {

    // delete the dynamically allocated player
//    delete pship;
//    pship = NULL;

//    delete guy;
//    guy = NULL;

    // delete all of the dynamically allocated ships
//    std::list<CPUActor*>::iterator actorsi;
//    for(actorsi=pCpuActors.begin();actorsi!=pCpuActors.end();actorsi++){
//        delete (*actorsi);
//        (*actorsi) = NULL;
//    }

}

bool TitleScreen::Init(Graphics* graphicsp, Sound* soundp, Controls* controlsp) {

    // if not already initialized...
    if (!m_bInitialized) {

        // save required pointers
        m_pGraphics = graphicsp;
        m_pSound = soundp;
        m_pControls = controlsp;

        // load background
        background = m_pGraphics->LoadSpritesheet( TitleBG );

        // create a ship to draw
        pship = new PlayerShip();
        pship->Init( Sprite(m_pGraphics->LoadSpritesheet(SpacePlayer),NULL), Point3D(0,0,0) );
        pship->setLocation(Point3D(50,50,0));
        pship->setMomentum(Point3D(0,0,0));

        // create a guy
        guy = new Guy();
        guy->Init( Sprite(m_pGraphics->LoadSpritesheet(DemoGuy),NULL), Point3D(0,0,0) );
        guy->setLocation(Point3D(500,300,0));
        guy->setMomentum(Point3D(2,0,0));

        // create some enemy ships
        SDL_Surface* eship_sheet = m_pGraphics->LoadSpritesheet(SpaceEnemy);
        for(int i=0; i < 20; i++) {
            EnemyShip* eship = new EnemyShip();
            eship->Init(Sprite(eship_sheet,NULL), Point3D(0,0,0) );
            eship->setLocation(Point3D(Random::GetRandomInt(50,ScreenWidth-50),-eship->getBounds().h,0));
            eship->setMomentum(Point3D(Random::GetRandomInt(-50,50),Random::GetRandomInt(10,200),0));
            pCpuActors.push_back(eship);
        }

        // declare as initialized
        m_bInitialized = true;

       return true;

    } else {

        // already initialized, not required.
        return false;

    }
}

bool TitleScreen::Update(Uint32 TimeDelta) {
    // update game state here
    // physics, sound effects, music, GameObject death, etc.

    //start music if it isn't started
    if (m_pSound->IsPlaying() == false) {
        m_pSound->PlayMusic(TitleMusic);
    }

    // accelerate the ship. Just because.
    guy->setMomentum( Point3D(guy->getMomentum().x*1.01,0,0) );

    // maximum speed
    int MAXSPEED = 500;
    if (guy->getMomentum().x > MAXSPEED)
        guy->setMomentum(Point3D(MAXSPEED,guy->getMomentum().y,guy->getMomentum().z));
    if (guy->getMomentum().x < -MAXSPEED)
        guy->setMomentum(Point3D(-MAXSPEED,guy->getMomentum().y,guy->getMomentum().z));

    // don't let the ship leave the screen on the right
    if (guy->getLocation().x+guy->getBounds().w >= 640) {
        guy->setLocation(Point3D(640-guy->getBounds().w,guy->getLocation().y,guy->getLocation().z));
        guy->setMomentum(Point3D(-2,0,0));
    }

    // don't let guy leave the screen on the left
    if (guy->getLocation().x <= 0) {
        guy->setLocation(Point3D(0,guy->getLocation().y,guy->getLocation().z));
        guy->setMomentum(Point3D(2,0,0));
    }

    // move the ship. Just to make it look like something is happening.
    //Physics::MoveObject(pship,TimeDelta);
    pship->setLocation( Point3D(m_pControls->getMouseX()-(pship->getBounds().w/2), m_pControls->getMouseY()-(pship->getBounds().h/2),0) );
    pship->update();

    // update the guy
    Physics::MoveObject(guy,TimeDelta);
    guy->update();

    // move and update the enemy ships
    std::list<CPUActor*>::iterator actorsi;
    for (actorsi=pCpuActors.begin();actorsi!=pCpuActors.end();actorsi++) {
        Physics::MoveObject((*actorsi),TimeDelta);
        (*actorsi)->update();
    }

    // delete ships that touch the edge
    // basically just to test removing items from a list
    //std::list<CPUActor*>::iterator actorsi;
    for (actorsi=pCpuActors.begin();actorsi!=pCpuActors.end();actorsi++) {
        if ( (*actorsi) ) {
        SDL_Rect loc = (*actorsi)->getBounds();
        if(loc.x <= 0 || loc.x >= ScreenWidth-loc.w || loc.y >= ScreenHeight-loc.h ) {
            delete (*actorsi); (*actorsi) = NULL; // safe delete the dynamically allocated ship
            std::list<CPUActor*>::iterator eraseme = actorsi; // create a new reference to the item to be deleted
            actorsi--; // back up the iterator to the previous element
            pCpuActors.erase(eraseme); // remove the ship pointer from the list
        }
        }
    }

    // end if the start button is pressed
    if ( m_pControls->getButton(BUTTON_START) )
        return true;

    // return true if the level is done, false if not.
    else
        return false;

}

bool TitleScreen::Render() {

    //TODO: Check if an object is Visible before rendering it.

    // draw the background
    m_pGraphics->SetBackground(background);
    m_pGraphics->RenderBackground();

    // draw all of the enemy ships
    std::list<CPUActor*>::iterator actorsi;
    for(actorsi=pCpuActors.begin();actorsi!=pCpuActors.end();actorsi++) {
        m_pGraphics->RenderSprite((*actorsi)->getSprite(),(*actorsi)->getLocation());
    }

    // render player
    m_pGraphics->RenderSprite(pship->getSprite(),pship->getLocation());
    m_pGraphics->RenderSprite(guy->getSprite(),guy->getLocation());

    // inform the player of how to begin the game
    m_pGraphics->BlitText("Welcome to my engine test app!",100,200);
    m_pGraphics->BlitText("Press Enter to play",175,250);

    return true;

}
