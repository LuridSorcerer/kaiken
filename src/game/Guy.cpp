/* Guy
 * Some guy we can use for platforming levels.
 */

#include "Guy.h"

SDL_Rect GUY_R_STAND = {  0,  0, 32, 32};
SDL_Rect GUY_R_WALK1 = { 32,  0, 32, 32};
SDL_Rect GUY_R_WALK2 = { 64,  0, 32, 32};
SDL_Rect GUY_R_WALK3 = { 96,  0, 32, 32};
SDL_Rect GUY_R_JUMP  = {128,  0, 32, 32};

SDL_Rect GUY_L_STAND = {  0, 32, 32, 32};
SDL_Rect GUY_L_WALK1 = { 32, 32, 32, 32};
SDL_Rect GUY_L_WALK2 = { 64, 32, 32, 32};
SDL_Rect GUY_L_WALK3 = { 96, 32, 32, 32};
SDL_Rect GUY_L_JUMP  = {128, 32, 32, 32};

bool Guy::Init(Sprite mySprite, Point3D myLocation) {

    // not but a babe
    age = 0;
    setAlive(true);

    // grab the sprite and location that were passed in
    sprite = mySprite;
    location = myLocation;

    // set a sprite clip
    sprite.clip = &GUY_R_STAND;

    // set facing
    facing = FACING_RIGHT;

    // done
    return true;

}

void Guy::update() {

    // increment age
    age++;

    // if he isn't moving, use standing sprite
    if (momentum.x == 0) {
        if (facing == FACING_LEFT) {
            sprite.clip = &GUY_L_STAND;
        } else {
            sprite.clip = &GUY_R_STAND;
        }
    }

    // determine which sprite should be used
    if (age%10==0 && momentum.x > 0) {
        if(sprite.clip==&GUY_R_STAND) {
            sprite.clip = &GUY_R_WALK1;
        } else if (sprite.clip==&GUY_R_WALK1) {
            sprite.clip = &GUY_R_WALK2;
        } else if(sprite.clip == &GUY_R_WALK2) {
            sprite.clip = &GUY_R_WALK3;
        } else {
            sprite.clip = &GUY_R_STAND;
        }
        facing = FACING_RIGHT;
    }
    if (age%10==0 && momentum.x < 0) {
        if(sprite.clip==&GUY_L_STAND) {
            sprite.clip = &GUY_L_WALK1;
        } else if(sprite.clip==&GUY_L_WALK1) {
            sprite.clip = &GUY_L_WALK2;
        } else if (sprite.clip==&GUY_L_WALK2) {
            sprite.clip = &GUY_L_WALK3;
        } else  {
            sprite.clip = &GUY_L_STAND;
        }
        facing = FACING_LEFT;
    }
    // if he's not on the ground, use a jumping sprite
    if (onGround == false) {
        if (facing == FACING_LEFT) {
            sprite.clip = &GUY_L_JUMP;
        } else {
            sprite.clip = &GUY_R_JUMP;
        }
    }

}

//SDL_Rect Guy::getBounds() {
//
//    SDL_Rect bounds;
//
//    bounds.h = sprite.clip->h;
//    bounds.w = sprite.clip->w-8;
//    bounds.x = location.x+4;
//    bounds.y = location.y;
//
//    return bounds;
//}

//void Guy::setLocation(Point3D newLocation) {
//    location = newLocation;
//    location.x+=4;
//}
