/*
    PlayerShip
    The player's ship during space shooter levels.

*/

#ifndef PLAYERSHIP_H
#define PLAYERSHIP_H

#include "../Kaiken.h"
//#include "Resources.h"

extern SDL_Rect PSHIP_1;
extern SDL_Rect PSHIP_2;
extern SDL_Rect PSHIP_3;
extern SDL_Rect PSHIP_4;

extern SDL_Rect PSHIP_DEAD_1;
extern SDL_Rect PSHIP_DEAD_2;
extern SDL_Rect PSHIP_DEAD_3;
extern SDL_Rect PSHIP_DEAD_4;

class PlayerShip : public Actor {

    protected:
    uint32_t age;

    public:
    PlayerShip() {}
    ~PlayerShip() {}

    bool Init(Sprite,Point3D);
    void update();

};

#endif
