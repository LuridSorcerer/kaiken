/* SpaceLevel
    A shameless and watered-down ripoff of Space Invaders.
    Just a basic Level.
*/

#ifndef SPACELEVEL_H
#define SPACELEVEL_H

#include "../Kaiken.h"
#include "game.h"

class EnemyShip;
class Bullet;

class SpaceLevel : public Level {

    // the ships, both enemies and player
    protected:
    std::list<EnemyShip*> eships;
    std::list<Bullet*> bullets;
    PlayerShip* pship;
    bool canShoot;
    SDL_Surface* bullet_sheet;

    public:

    // constructor/deconstructor
    SpaceLevel();
    ~SpaceLevel();

    // functions needed for running
    virtual bool Init(Graphics*,Sound*,Controls*);
    virtual bool Update(Uint32);
    virtual bool Render();
};

#endif
