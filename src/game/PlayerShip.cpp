/*
    PlayerShip
    The player's ship during space shooter levels.

*/

#include "PlayerShip.h"

SDL_Rect PSHIP_1 = {0,0,32,32};
SDL_Rect PSHIP_2 = {32,0,32,32};
SDL_Rect PSHIP_3 = {64,0,32,32};
SDL_Rect PSHIP_4 = {96,0,32,32};

SDL_Rect PSHIP_DEAD_1 = {0, 32,32,32};
SDL_Rect PSHIP_DEAD_2 = {32,32,32,32};
SDL_Rect PSHIP_DEAD_3 = {64,32,32,32};
SDL_Rect PSHIP_DEAD_4 = {96,32,32,32};

bool PlayerShip::Init(Sprite mySprite, Point3D myLocation) {

    // brand new
    age = 0;

    // set sprite and location based on value passed in
    sprite=mySprite;
    location=myLocation;

    // set beginning animation frame
    sprite.clip = &PSHIP_1;

    setAlive(true);

    return true;

}

void PlayerShip::update() {

    // keep track of how long the player has been alive
    // useful for doing animation
    age++;

    // Idle animation
    if (age%10 == 0) {
        if (sprite.clip == &PSHIP_1) {
            sprite.clip = &PSHIP_2;
        }
        else if (sprite.clip == &PSHIP_2) {
            sprite.clip = &PSHIP_3;
        }
        else if (sprite.clip == &PSHIP_3) {
            sprite.clip = &PSHIP_4;
        }
        else if (sprite.clip == &PSHIP_4) {
            sprite.clip = &PSHIP_1;
        }
    }

    // Death animation
    if (age%10 == 0 && isAlive()==false) {
        if (sprite.clip == &PSHIP_DEAD_1) {
            sprite.clip = &PSHIP_DEAD_2;
        }
        else if (sprite.clip == &PSHIP_DEAD_2) {
            sprite.clip = &PSHIP_DEAD_3;
        }
        else if (sprite.clip == &PSHIP_DEAD_3) {
            sprite.clip = &PSHIP_DEAD_4;
        }
        else if (sprite.clip == &PSHIP_DEAD_4) {
            sprite.clip = &PSHIP_1;
            setAlive(true); // just for now...
        }
//        else {
//            sprite.clip = &PSHIP_DEAD_1;
//        }
    }


}
