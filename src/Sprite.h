/* Sprite
 *
 * Holds the information that a
 * sprite will need for rendering, including the sprite sheet, sprite
 * clip from the sheet, and location on-screen.
 */

#ifndef SPRITE_H
#define SPRITE_H

struct Sprite {

    Sprite() { }
    Sprite(SDL_Surface* newSheet, SDL_Rect* newClip) {
        sheet = newSheet; clip = newClip; }

	// sprite sheet
	SDL_Surface* sheet;

	// the part of the sprite sheet to use
	SDL_Rect* clip;

	// x and y coordinates
	int x, y; // DEPRECATED, don't use plox
};

#endif
