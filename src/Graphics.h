/* Graphics
 *
 * This class holds functions for drawing crap on the screen.
 */

#ifndef GRAPHICS_H
#define GRAPHICS_H

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <string>

#include "Point3D.h"
#include "Sprite.h"

class Graphics {

	private:
		int m_iWidth;
		int m_iHeight;
		SDL_Surface* mScreen;
		SDL_Surface* mBG;
		TTF_Font* mFont;

	public:
		Graphics();
		~Graphics();

		bool Init(int width, int height, SDL_Surface* screen);

		bool LoadBackground(std::string filename);
		SDL_Surface* LoadSpritesheet(std::string filename);

		bool SetBackground(SDL_Surface*);

		bool RenderBackground();
		bool RenderSprite(Sprite renderme);
		bool RenderSprite(Sprite renderme, Point3D where);

		bool LoadFont(std::string filename);
        bool BlitText(std::string text,int x, int y);

		bool Render();

		void Quit();
};

#endif
