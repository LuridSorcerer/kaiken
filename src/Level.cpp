/* Level.h
    Defines the Level class.
    Levels have a list of Blocks, a list of Actors and a Player.

    Hopefully, later on they'll include a list of events or something.
    But first I'll need some method of triggering events and whatnot,
    so we won't worry about that yet.
*/

#include "Level.h"
#include <sstream>
#include <iostream>

Level::Level() {

    // init those member variables!
    m_pGraphics = NULL;
    m_pSound = NULL;
    m_pControls = NULL;

    m_bInitialized = false;

}

bool Level::Init(Graphics* graphicsp, Sound* soundp, Controls* controlsp) {

    // if not already initialized...
    if (!m_bInitialized) {

        // save required pointers
        m_pGraphics = graphicsp;
        m_pSound = soundp;
        m_pControls = controlsp;

        // declare as initialized
        m_bInitialized = true;

        return true;

    } else {

        // already initialized, not required.
        return false;

    }
}

bool Level::Update(Uint32 TimeDelta) {
    // update game state here
    // physics, sound effects, music, GameObject death, etc.

    // end if the start button is pressed
    if ( m_pControls->getButton(BUTTON_START) )
        return true;

    // return true if the level is done, false if not.
    else
        return false;

}

bool Level::Render() {

    //TODO: Check if an object is Visible before rendering it.

    // draw the background
    m_pGraphics->SetBackground(background);
    m_pGraphics->RenderBackground();

    // draw all of the blocks
    std::list<Block>::iterator blocksi;
    for (blocksi=blocks.begin(); blocksi!=blocks.end(); blocksi++) {
        m_pGraphics->RenderSprite( blocksi->getSprite(), blocksi->getLocation() );
    }

    // draw all of the non-player actors
    std::list<CPUActor>::iterator actorsi;
    for (actorsi=cpuActors.begin(); actorsi!=cpuActors.end(); actorsi++) {
        m_pGraphics->RenderSprite( actorsi->getSprite() , actorsi->getLocation() );
    }


    // inform the player of how to begin the game
    m_pGraphics->BlitText("Welcome to my engine test app!",100,200);
    m_pGraphics->BlitText("Press Enter to begin",200,250);

    return true;

}
