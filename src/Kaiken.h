/* Kaiken
 *
 * This class holds all of the data that the program uses and contains
 * input collection and the game loop.
 */

/* Mostly incomplete Changelog
 *
 *  2012-12-28:
 *      Huge cleanup of old-ass code
 *      Switched to SDL 1.2.15
 *      Separated Kaiken code and game code
 *      Renamed Application (files, classes, etc.) to Kaiken
 *      Removed Bullet class (implemented new in game code)
 *      Blossomings of a config. file (fullscreen.txt, only one option)
 *
 */

#ifndef KAIKEN_H
#define KAIKEN_H

#include <SDL/SDL.h>

#include <list>
#include <vector>

#include "InputCore.h"
#include "Window.h"
#include "Sound.h"
#include "Graphics.h" /* Included in GameObject.h */
#include "Random.h"
#include "GameObject.h"
#include "Controls.h"
#include "Level.h"
#include "Physics.h"

#include "Resources.h"

#include "game/game.h"

const int ScreenWidth = 640;
const int ScreenHeight = 480;

class Kaiken : public InputCore {

	private:
		// we'll add things here as we go
		bool m_bDone;

		Window mWindow;
		Sound mSound;
		Graphics mGraphics;
		Random mRandom;
		Controls ctrls;

        std::vector<Level*> levels;
        uint32_t currentLevel;

        //bool m_bMouseCtl;

		Uint32 m_iFrameCount;

		Uint32 m_iTime;

		bool m_bSoundEnabled;

	public:
		Kaiken();
		~Kaiken();

		bool Init();
		void Run();
		void TakeInput(SDL_Event* pEvent);
		void Update();
		void Render();
		void Quit();

		// "overloaded" input functions
		void OnResize(int width, int height);
		void OnExit();

		void OnKeyPress(SDLKey key, SDLMod mod, Uint16 unicode);
		void OnKeyRelease(SDLKey key, SDLMod mod, Uint16 unicode);

		void OnMouseMotion(int mX, int mY, int relX, int relY,
			bool left, bool middle, bool right);
        void OnMousePress(Uint8 button, int x, int y);
		void OnMouseRelease(Uint8 button, int x, int y);
};

#endif

