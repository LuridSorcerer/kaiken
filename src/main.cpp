/* main.cpp
 *
 * This file basically starts creates an Application object, initializes
 * it, runs it, then quits it. That's pretty much it.
 */

//#include <iostream>
#include "Kaiken.h"

//Extern'd in Kaiken.h
Kaiken* pApp;

int main(int argc, char* argv[]) {

	// create the application on the heap
	pApp = new Kaiken();

	// initialize the application. Quit if it fails
	if(pApp->Init() != true) { return -1; }

	// run the application
	// in Run(), it will hangle Input, Logic and Rendering
	pApp->Run();

	// clean up application, prepare to quit
	pApp->Quit();

	// make sure the Application pointer isn't NULL, then deallocate it
	// set it to NULL after deallocation
	if(pApp) {
		delete pApp;
		pApp = NULL;
	}

	// all done, quit
	return 0;

}
