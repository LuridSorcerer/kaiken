/* Random
 *
 * A random number generator class.
 *
 * TODO:
 *   Create a better scheme of generating random numbers
 *   Allow for getting/setting of seed value
 */

#ifndef RANDOM_H
#define RANDOM_H

#include <math.h>
#include <time.h>
#include <stdlib.h>

/// TODO: 	add functions for setting and getting the random seed
///			get a better random number generator

class Random {

	public:
		Random();
		~Random();

		bool Init();

		static int GetRandomInt(int min, int max);

};

#endif
