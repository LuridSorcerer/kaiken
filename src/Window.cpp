#include "Window.h"
#include <iostream>

Window::Window() {
	
	// basic constructor, init variables
	
	m_iWidth = 0;
	m_iHeight = 0;
	m_iBPP = 0;
	m_iFlags = 0;
	mWindow = NULL;
	mTitle = "Untitled Window";
}

Window::~Window() {
	// destructor, deallocate dynamically allocated variables
}

int Window::GetWidth()  const {  return m_iWidth; }
int Window::GetHeight() const { return m_iHeight; }
int Window::GetBPP()    const {    return m_iBPP; }
SDL_Surface* Window::GetSurface() const { return mWindow; }
std::string Window::GetTitle() const { return mTitle; }

void Window::SetTitle(std::string title) {
		
		mTitle = title;
		
		SDL_WM_SetCaption(mTitle.c_str(), NULL);
		
}

bool Window::Init(int width, int height, int bpp, Uint32 flags) {
	
	// grab data that was passed in
	m_iWidth = width;
	m_iHeight = height;
	m_iBPP = bpp;
	m_iFlags = flags;
	
	
	// create the window, return false if it failed
	if((mWindow = SDL_SetVideoMode(m_iWidth,m_iHeight,m_iBPP,m_iFlags)) == NULL) {
		return false;
	}
	
	// success, return true
	return true;
	
}

bool Window::Resize(int width, int height) {
	
	m_iWidth = width;
	m_iHeight = height;
	
	// try to resize the window. return false if it fails.
	if((mWindow = SDL_SetVideoMode(m_iWidth,m_iHeight,m_iBPP,m_iFlags)) == NULL) {
		return false;
	}
	
	// success, return true
	return true;
}
