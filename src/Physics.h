/* Physics
 * A class, chiefly of static functions, that performs various operations
 * related to physics.
 */

#ifndef PHYSICS_H
#define PHYSICS_H

#include <vector>
#include <list>
// Includes the objects we'll be comparing, as well as
// classes like Point3D (from Graphics.h).
#include "GameObject.h"

const int TICKS_PER_SECOND = 1000;

/* CollisionType
 * An easy way to tell from where one object is colliding with another.
 */
enum CollisionType {
    COLLISION_NONE,
    COLLISION_TOP,
    COLLISION_LEFT,
    COLLISION_RIGHT,
    COLLISION_BOTTOM
};

/* Circle
 * Holds data relevant to circles, and is used in circle collision
 * detection.
 */
struct Circle {
    Point3D center;
    float radius;
};

class Physics {

    public:

    static CollisionType CheckBoxCollision(GameObject* mover,GameObject* target);
    static CollisionType CheckCircleCollision(GameObject* mover,GameObject* target);
    static void CheckBlockCollisions(Actor* mover, std::vector<Block> blocks);
    static void CheckBlockCollisions(Actor* mover, std::list<Block*> blocks);

    static bool ApplyFriction(Actor*,Uint32);
    static bool ApplyGravity(Actor*,Uint32);
    static bool MoveObject(GameObject*,Uint32);

};

#endif
