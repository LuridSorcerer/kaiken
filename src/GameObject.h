/* GameObject:
    Game objects are objects that appear in the game. I KNOW! Ingenious,
    right?! They can be blocks, enemy players, or even the player's character.
    It is basically an interface; there are no GameObject objects, only
    objects of classes that inherit from GameObject.
*/

#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

// Sprite and Point3D are in Graphics.h
#include "Graphics.h"

/* ========================================================== */
/* ======================= GameObject ======================= */
/* ========================================================== */

// GameObject: A class of objects that appear in the game. Or rather, they
// don't, but objects of classes derived from this class do.
class GameObject {

    protected:
    Point3D location;
    Point3D prevLocation;
    Point3D momentum;
    Sprite sprite;
    SDL_Rect bounds; // will be calculated on-the-fly?
    bool visible;

    public:
    GameObject() { }
    //~GameObject() { SDL_FreeSurface(sprite.sheet); }

    // getters
    virtual SDL_Rect getBounds();
    Point3D  getLocation();
    Point3D  getPrevLocation();
    Point3D  getMomentum();
    Sprite   getSprite();
    bool     getVisible();

    // setters
    //void setBounds(SDL_Rect); // doubt I'll need this.
    virtual void setLocation(Point3D); // change these Point3Ds into Point3D*s?
    void setPrevLocation(Point3D);
    void setMomentum(Point3D);
    //void setSprite(Sprite);
    void setVisible(bool);

};

/* ========================================================== */
/* ========================== Block ========================= */
/* ========================================================== */

// Block: A GameObject with no free will.
class Block : public GameObject {

    /* There may be a point in time when I decide that Blocks are more than just
     * GameObjects. Thus, a separate class. However, that time has not arrived,
     * yet, and so there are no differences yet. */

    public:
    Block() { }
    bool Init(Sprite, Point3D location);

};

/* ========================================================== */
/* ========================== Actor ========================= */
/* ========================================================== */

// Actor: Creatures in the game that move, either through AI or player control.

class Actor : public GameObject {

    protected:
    bool alive;
    bool onGround;
    //Point3D previousLocation;

    /* DEBUG: For testing animation technique */
//    SDL_Rect ship1;
//    SDL_Rect ship2;


    public:

    Actor() { }
    bool Init(Sprite, Point3D location);

    bool isAlive();
    bool isOnGround();
    void setAlive(bool);
    void setOnGround(bool);
    virtual void update();

};

/* ========================================================== */
/* ========================= CPUActor ======================= */
/* ========================================================== */

// CPUActor: An actor controlled by AI.

class CPUActor : public Actor {

    protected:
    // certainly, it'll need to hang on to some data to know what it
    // needs to do. Perhaps a pointer to the player (or at least the
    // player's location, whether or not it can fire and how much
    // ammo it has left.

    public:
    CPUActor() { }
    virtual ~CPUActor() {}
    bool Init(Sprite, Point3D Location);

    void tickAI(); // this functon should be called each frame to allow the
                   // actor to sense its surroundings and act accordingly.
    virtual void update() {}
};

/* ========================================================== */
/* ========================== CPUShip ======================= */
/* ========================================================== */

class CPUShip : public CPUActor {

    public:
    void tickAI();

};

/* ========================================================== */
/* ========================== Bullet ======================== */
/* ========================================================== */

/*
class Bullet : public Actor {

    public:
    GameObject* owner;

    bool Init(Sprite,Point3D myLocation, Point3D myMomentum,GameObject* myOwner);
    GameObject* getOwner();
    bool setOwner (GameObject* myOwner);

    ///IDEA:
    ///Create a hit(Actor*) function that applies an effect directly to the actor that collided with the bullet.
    ///It might be best, however, to let the level handle this. The Level has a lot more information about the actor and bullet.

};
*/

#endif
