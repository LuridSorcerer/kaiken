/* ========================================================== */
/* ======================== Point3D ========================= */
/* ========================================================== */

#ifndef POINT3D_H
#define POINT3D_H

// Point3D: A simple struct that holds 3D coordinates.
struct Point3D {

    public:

    float x, y, z;

    // default constructor: set all to zero
    Point3D()
        { x = 0.0f; y = 0.0f; z = 0.0f;}

    // 3-float constructor: set all axes to passed-in value
    Point3D(float startx,float starty,float startz)
        {x = startx; y = starty; z = startz;}

    // TODO: += and -= operators

    Point3D operator+(const Point3D&) const;
    Point3D operator-(const Point3D&) const;

};

#endif
