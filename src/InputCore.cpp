#include "InputCore.h"

void InputCore::OnEvent(SDL_Event* pEvent) {
	
	// This function repeatedly fills pEvent with events and then handles
	// them. It does this until no more events remain.
	
	switch(pEvent->type) {
		
		SDL_keysym sym;
		
		// key pressed
		case SDL_KEYDOWN:
			sym = pEvent->key.keysym;
			OnKeyPress(sym.sym, sym.mod,sym.unicode);
			break;
		
		// key released
		case SDL_KEYUP:
			sym = pEvent->key.keysym;
			OnKeyRelease(sym.sym, sym.mod,sym.unicode);
			break;
			
		// mouse moved
		case SDL_MOUSEMOTION:
			// condense like SDL_KEY[UP,DOWN]?
			OnMouseMotion(pEvent->motion.x, pEvent->motion.y, 
				pEvent->motion.xrel, pEvent->motion.yrel,
				(pEvent->motion.state & SDL_BUTTON(SDL_BUTTON_LEFT)) != 0,
				(pEvent->motion.state & SDL_BUTTON(SDL_BUTTON_MIDDLE)) != 0,
				(pEvent->motion.state & SDL_BUTTON(SDL_BUTTON_RIGHT)) != 0);
			break;
		
		// mouse button pressed
		case SDL_MOUSEBUTTONDOWN:
			OnMousePress(pEvent->button.button, pEvent->button.x, pEvent->button.y);
			break;
		
		// mouse button released
		case SDL_MOUSEBUTTONUP:
			OnMouseRelease(pEvent->button.button, pEvent->button.x, pEvent->button.y);
			break;
		
		// focus changed
		case SDL_ACTIVEEVENT:
			OnFocusChange(pEvent->active.state,pEvent->active.gain);
			break;
		
		// window resized
		case SDL_VIDEORESIZE:
			OnResize(pEvent->resize.w, pEvent->resize.h);
			break;
		
		// window closed
		case SDL_QUIT:
			OnExit();
			break;
			
		// default
		default: ;
			
	}
	
}
