/*  Controls: a structure for holding which controls are being employed.
    Typically, this would refer to buttons. The data in this class,
    however, is decoupled from the actual button-reading mechanisms.
*/

#ifndef CONTROLS_H
#define CONTROLS_H

#include "Graphics.h"

enum Button {
    BUTTON_UP=0,
    BUTTON_DOWN,
    BUTTON_LEFT,
    BUTTON_RIGHT,
    BUTTON_START,
    BUTTON_SHOOT,
    BUTTON_DEBUG,
    BUTTON_COUNT // this isn't a button. It's an easy way to
                 // count the number of supported buttons.
};

/* Controls struct.
    It holds booleans for each button that may or may not be down as well as
    the location of the mouse pointer.
*/


class Controls {

    private:
    bool buttons[BUTTON_COUNT];
    int  mouseX, mouseY;

    public:
    Controls();
    void setButton(Button btn, bool pressed);
    bool getButton(Button btn);
    void setMousePosition(int x, int y);
    int getMouseX();
    int getMouseY();
    Point3D getMousePosition();

};

#endif
