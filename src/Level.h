/* Level.h
    Defines the Level class.
    Levels have a list of Blocks, a list of Actors and a Player.

    Hopefully, later on they'll include a list of events or something.
    But first I'll need some method of triggering events and whatnot,
    so we won't worry about that yet.
*/

#ifndef LEVEL_H
#define LEVEL_H

#include <list>
#include <vector>

#include "Graphics.h"
#include "Sound.h"
#include "Controls.h"
#include "GameObject.h"
#include "Physics.h"

#include "Resources.h"

class Level {

    // My layers idea will likely be implemented here at some point. It goes as such:
    // Bottom: Background (image or color)
    //         Background decorations (think of clouds in Super Mario Bros.)
    //         Blocks
    //         Block Decorations
    //         Sprites
    //         Top Decorations
    // Top:    HUD
    //
    // These will be defined in classes inhereted from this one. For now, we
    // won't worry about these much.

    protected:

    // Pointers to required objects
    Graphics* m_pGraphics;
    Sound* m_pSound;
    Controls* m_pControls;

    // already initialized?
    bool m_bInitialized;

    // background
    SDL_Surface *background;

    // list of Blocks
    std::list<Block> blocks;

    // list of Actors (AI enemies,etc.)
    std::list<CPUActor> cpuActors;

    // TODO: Probably want to add a list of Bullets.
    // I made the Bullet class, right?...

    // player
    Actor player;

    public:
    Level(); // A constructor should be added that reads from a file. this will be
             // useful once a level file format has been created.
    virtual bool Init(Graphics*,Sound*,Controls*); // returns true if initialized successfully,
             // returns false if wasn't required. For hard-coded levels, the blocks and
             // actors will be defined and added to their respective lists here.
    virtual bool Update(Uint32); // returns whether or not the level is done.
    virtual bool Render(); // returns true. May be used in the future.

};

#endif
