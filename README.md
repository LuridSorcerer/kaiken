Kaiken, a simple 2D game engine

Kaiken started as a hobby project, just an experiment on creating a workable 2D game environment. It supports mulitple levels, music, and some animation.

It was written using SDL for graphics, sound, and input. Work on it was discontinued in 2014. Since the start, SDL2 has been released, as well as the C++ 14 standard. Instead of starting over or porting the project to the new technology, I instead lost interest and moved on. 

The project is slighly difficult to build. It is a CodeBlocks project, and there are bugs that cause it crash. These issues did not appear to be present using older compilers, and I don't really have the time or inclination to track down what's causing them. 

The project is kept for historical and sentimental reasons more than anything else. Perhaps one day I'll find the time and motivation to code a new engine or clean this one up.

--Andrew Lingenfelter